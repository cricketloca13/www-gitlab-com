- name: Enablement - Section PPI, Stage PPI - Median End User Page Load Time
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user page performance collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: 1.3
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - Median page load times have not changed since April, however the outliers (p90 and p95) have slightly decreased.
      - Now [Median](https://app.periscopedata.com/app/gitlab/790506/gitlab.com-performance-per-snowplow-dashboard?widget=12056437&udv=0) and [p90](https://app.periscopedata.com/app/gitlab/790506/gitlab.com-performance-per-snowplow-dashboard?widget=12057815&udv=0) load times by country by week are available. 
  implementation:
    status: Complete
    reasons:
      - We are still working to [improve our per-geo views](https://gitlab.com/gitlab-data/analytics/-/issues/8014).
  lessons:
    learned:
    - Page load times are not significantly affected by geography, with median times of 1.1s for US, 1.2 for most of Europe, and 1.3 for Australia. It seems internet latency is not a significant driver of overall performance for .com, however pages with many async requests will see higher impact. 
    - We are working to [directly tie page load data to routes](https://gitlab.com/gitlab-org/gitlab/-/issues/331807), with some of this content already sent but [not processed in our data pipeline](https://gitlab.com/gitlab-data/analytics/-/issues/9440). 
    - We are also working to include RUM data (frontend performance) in error budgets, but pulling it into Grafana has proven [more difficult](https://gitlab.com/gitlab-data/analytics/-/issues/8733#note_603067403) than originally thought. We will need to update our lambda to push to Prometheus. 
    - Also exploring a [CI test suite](https://gitlab.com/gitlab-org/gitlab/-/issues/331808) to catch regressions before they make it to production.
    urls:
      - https://gitlab.com/gitlab-data/analytics/-/issues/5657
  metric_name: performanceTiming
  sisense_data:
    chart: 10546836
    dashboard: 794513
    embed: v2
  sisense_data_secondary:
    chart: 10603331
    dashboard: 794513
    embed: v2

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - % on latest 3 versions has been stable, median age of version has returned to increasing trend.
    - More research is required to understand reasoning for jump in age across all versions and editions.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
  lessons:
    learned:
    - The [most recent update](https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=8458214&udv=1059380) to the median age of license shows an increase across the board. This [will continue to be investigated and discussed](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/821) to see how we can trend downward again. A new research topic to explore is, [how can we prompt community edition users to upgrade more regularly](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/844), since they are notably the largest median age.
    - Security was a central focus for admins to upgrade their version from the [ease of upgrading study](https://gitlab.com/gitlab-org/ux-research/-/issues/1114). The inital MVC has been merged, to [prompt users to sign up for the security newsletter](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/62502) in the next steps docs after installation. The next iteration will be to [prompt current users to sign up for the security newsletter in the admin console](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/842).
    - Another important aspect for admins to upgrade was ["when developers are asleep"](https://gitlab.com/gitlab-org/ux-research/-/issues/1114#note_515385825), or less active within an organization. The MVC will be to [display this activity to admins](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/845), and showcase the time of least activity. 
    - Work has begun on [better highlighting](https://gitlab.com/gitlab-org/gitlab/-/issues/295266) when instances are out of date to administrators. This will impact results once it has been completed, estimated for this milestone.
    - Based on the [ease of upgrading study](https://gitlab.com/gitlab-org/ux-research/-/issues/1114) admins also were open to being notified more prominently when an upgrade was available. Work has begun on creating MVC's for a few [new ways to notify admins](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/843) in their preferred communication channel, that an upgrade is available.
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10278985&udv=1102166
  - https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=10279895&udv=1102166

- name: Enablement:Distribution  - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget for Distribution is currently WIP. The idea being worked on is a pipeline health error budget. The metric to track could be number of pipeline failures per month, then (more) aggressively prioritize pipeline related issues until we're able to consistently able to stay below the threshold. We could trend pipeline failures over time to show tangible progress. 
  target: TBD
  org: Product
  public: true
  is_key: false
  health:
    level: 2
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-distribution/stage-groups-group-dashboard-enablement-distribution)

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric helps us understand whether end users are actually seeing value in, and are using, geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    -  We've enabled [gathering usage data from Geo secondaries](https://gitlab.com/groups/gitlab-org/-/epics/4660) and have [added git fetch metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/298781) and [git push metrics](https://gitlab.com/gitlab-org/gitlab/-/issues/320984). Metrics are not yet available; the data team is [working on getting them plotted in Sisense](https://gitlab.com/gitlab-data/analytics/-/issues/8766).
    - GMAU is currently based on logging into the Secondary web interface. Git access is more common and we are tracking Git push and pulls as of 14.0 (see above). We also know that the UX of the secondary web interface is not good and we want to remove it, see [Opportunity Canvas(internal)](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit#heading=h.4mt5fmtn0ax4). In order to assess the impact of our planned changes, having this (low) GMAU is really important. Implementation work is in progress and can be tracked in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
    -  For Disaster Recovery we are measuring *potential Geo users* - the number of active licenses. In an ideal world, no regular user would ever need to rely on Geo because there is no disaster, but if one occurs everyone benefits. [MR is being reviewed](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/84904) to turn this into a separate PPI
    - We are working to add support for replicating all data types, so that Geo is on a solid foundation for both DR and Geo Replication.
    - In 14.0 we replicate ~86% of all data types (25 out of 29 in total) and verify ~48%. Shipped [Terraform State File verification](https://about.gitlab.com/releases/2021/05/22/gitlab-13-12-released/#geo-verifies-replicated-terraform-state-files)
 
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com database.
  lessons:
    learned:
    - The number of users is still very low, likely because the WebUI is insufficient. We are planning to change this, see [this opportunity canvas](https://docs.google.com/document/d/1S27A6u134ASCZT_pcKHuxJrUA0aZybxfuHIci1FhYHg/edit?usp=sharing) and in-progress implementation work in the [secondary mimicry epic](https://gitlab.com/groups/gitlab-org/-/epics/1528).
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/4660
  - https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2

- name: Enablement:Geo - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-geo/stage-groups-group-dashboard-enablement-geo)
    - Geo is not deployed on .com - This currently monitors one sidekiq worker that checks whether Geo is enabled.

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Working to adjust chart to include [how many of each process are started by default](https://gitlab.com/gitlab-com/Product/-/issues/1744) to better represent a default configuration.
    - Plot displays a 13.13 release for unknown reasons. Investigating
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - Memory consumption remains stable; 14.0 data to early to analyze.
    - In collaboration with the Secure stage the team reduced the memory impact for the worst 1% of calls [to the security report endpoint from 7GB down to 21MB](https://gitlab.com/gitlab-org/gitlab/-/issues/330122#note_594048895),a 99.7% reduction. This shows that increasing visibility of problems plus collaboration can make a huge difference!
    - By moving a subset of SideKiq workers from the primary database to read-only secondaries, the team removed [about 10% of transactions from the primary PostgreSQL database](https://gitlab.com/groups/gitlab-org/-/epics/5691#note_606250739 ) - even on busy days. This is important because it increases the stability and performance of GitLab.com
    - To further reduce memory consumption, we are planning to work on [splitting the application into functional parts to ensure that only needed code is loaded](https://gitlab.com/groups/gitlab-org/-/epics/5278) and on [reducing the memory consumption for Puma and Sidekiq endpoints](https://gitlab.com/groups/gitlab-org/-/epics/5622).

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - In May, Paid GMAU is up by 9%. SaaS up 10%, Self-Manged up 9% 
    - There is contiuning growth of more than 10% MoM since Oct 2020
    - Advanced Search performance remains stable since scaling up to 12 nodes.  
    - UI enhancements planned for the next month to address top SUS feedback will help accelerate growth
  implementation:
    status: Complete
    reasons:
    - Data collected is incomplete for September and October 2020 and January 2021 
  lessons:
   learned:
    - The [Recently Searched/Used Projects and Groups will be added to the dropdowns on the search results page.](https://gitlab.com/gitlab-org/gitlab/-/issues/239384/?version=216030) Our customers have been requesting this feature for over 5 years. 
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Global Search - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-global_search/stage-groups-group-dashboard-enablement-global-search)
    - Error Budget was Exceeded for Global Search in May 2021. It has been discovered that the priamry reason causing this is [Search Count calls.](https://gitlab.com/gitlab-org/gitlab/-/issues/331866)
    
- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - The query Apdex has decreased in all charts after the release of 13.10, including [the weekly one](https://app.periscopedata.com/app/gitlab/754160/Enablement::Database---Performance-Indicators?widget=9885649&udv=0), which has a pretty constant number of instances reporting stats. No similar trend on the GitLab.com charts, nor any other clear signal indicating what may be affecting the Apdex. This is still present in 13.12 but the team has not focused on an investigation due to capacity constraints. Current focus is mitigating the Primary Key Overflow risk.
    - We expect the updates performed in the scope of [Research Database queries for performance and frequency](https://gitlab.com/groups/gitlab-org/-/epics/5652) to further improve the ratio of queries which complete within 250ms and lower the variance of the Apdex even further.
    - We expect our work on [automated database migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6) to indirectly improve the database performance by minimising the number of database related incidents during deployments.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4.
  lessons:
    learned:
    - Apdex on GitLab.com exceeds our group PPI (see [100ms - Tolerable 250ms](https://tinyurl.com/64e6acku) and [50ms - Tolerable 100ms](https://tinyurl.com/4mjw5azv) for master) but reflects with sharp drops the production incidents that related to the database.
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Database - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-database/stage-groups-group-dashboard-enablement-database)    

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 134000
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Paid Monthly Active Users on GitLab.com increased by 4.3% in May to 130K, on track for our Q2 target.
    - Net Retention on GitLab.com [remains above](https://app.periscopedata.com/app/gitlab/710777/Infra-PM-Dashboard?widget=9417810&udv=1113414) the 130% target while Gross Retention is below the 90% target due to a dip starting in January 2021. We created a [dashbord](https://app.periscopedata.com/app/gitlab/710777/Infra-PM-Dashboard?widget=12057557&udv=1113414) to investigate further and found that the drop was driven by a Large customer whose spend contracted this year citing uncertainty in their industry due to the pandemic and an SMB customer who decided to downgrade to Premium because they weren't using the Ultimate feature set. We expect Gross Retention to stay in the 85% range through the end of the calendar year since the metric is pulling in last year's spend from these two customers and comparing it to their current spend on a monthly basis. We will continue to monitor retention as a supporting metric for Paid GMAU moving forward.
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  lessons:
    learned:
    - Need to account for seasonal fluctuations into target projections. 
  metric_name: COUNT_EVENTS_ACTIVE_USERS_LAST_28_DAYS_BY_PLAN_WAS_PAID
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2

- name: Enablement:Infrastructure - Error Budget for GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The error budget provides a clear, objective metric that determines how unreliable the service is allowed to be within a single quarter. Spent budget is the time (in minutes) during which user facing services have experienced a percentage of errors below the specified threshold and latency is above the specified objectives for the service.
  target: 99.95% availability, allowed unavailability window is 20 minutes per month
  org: Product
  public: true
  is_key: false
  health:
    level: 1
    reasons:
    - This is a new PI and is a shared PI with [Infrastructure availability targets](/handbook/engineering/infrastructure/performance-indicators/#gitlabcom-availability)
    - The [dashboards are located in grafana](https://dashboards.gitlab.net/d/stage-groups-infrastructure/stage-groups-group-dashboard-enablement-infrastructure)   
