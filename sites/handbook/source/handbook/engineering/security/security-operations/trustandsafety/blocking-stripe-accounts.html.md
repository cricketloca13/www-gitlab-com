---
layout: handbook-page-toc
title: "Blocking of Stripe Accounts"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Overview

This page stipulates the workflow steps that need to be followed when we blocking a Stripe account. We may block Stripe accounts for several reasons including to [Prevent Crypto-Mining Abuse](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/). 

## Workflow

### How Payment Validation Works

1. In Stripe, a charge for $1.00 is made and is either Payment authorized or Payment declined
1. For those where payment is authorized, Stripe performs a payment cancellation to reverse the charge
1. In both cases (authorized and declined) a record is created and stored in stripe with a fingerprint
1. In Stripe, we can view attributes (some of which are not searchable but are viewable in the UI) including:
    1. Date/time
    1. Payment Method (Visa, MC, etc)
    1. Last four digits of the card
    1. CC origin
    1. CC Issuer
    1. Expiration date
    1. Customer Name
1. When we authorize credit cards we do not currently store any attributes about the card anywhere on our side, just simply approved/denied.
1. In Stripe, we can manually block a card which passed a successful verification test using the process documented below.
1. We could consider a future dev issue to store some meta data about the credit card authorization on our side for easier (or perhaps automated) blocking in the future.

### Blocking a Credit Card in Stripe

1. Log into GitLab Admin: https://gitlab.com/admin/users/"username" and view the user details of the user you intend to block. Take note of the Credit Card validated at timestamp.
1. Log into Stripe: https://dashboard.stripe.com/login and navigate to All transactions under Payments.
1. Filter the transactions using the search bar: `status:canceled`.
1. Search for the record using date/time of the verification.
1. Select the desired record.
1. Select Add to block list on top right-hand corner.

![](gitlab_admin_user_details.png)
![](stripe_search_payments.png)
![](stripe_payment_details.png)
